package utils_test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

import simulation.utils.ParamsUtils;

public class ParamUtilsTest {

	@Test 
	public void binaryVariationsLengthTest() {
		assertEquals(2, ParamsUtils.permutate(1).size());
		assertEquals(4, ParamsUtils.permutate(2).size());
		assertEquals(8, ParamsUtils.permutate(3).size());
		assertEquals(16, ParamsUtils.permutate(4).size());
		assertEquals(32, ParamsUtils.permutate(5).size());
		assertEquals(1024, ParamsUtils.permutate(10).size());
		
		
		/*System.out.println(ParamsUtils.permutate(1).toString());
		System.out.println(ParamsUtils.permutate(2).toString());
		System.out.println(ParamsUtils.permutate(3).toString());
		*/
	}
	
	@Test
	public void oneChangeVariationsTest() { 
		ArrayList<Boolean> input = new ArrayList<>(Arrays.asList(true, true, true));
		ArrayList<ArrayList<Boolean>> expect = new ArrayList<>();
		expect.add(new ArrayList<Boolean>(Arrays.asList(false, true, true)));
		expect.add(new ArrayList<Boolean>(Arrays.asList(true, false, true)));
		expect.add(new ArrayList<Boolean>(Arrays.asList(true, true, false)));
		assertEquals(expect, ParamsUtils.getOneChangeVariations(input));
		
	}
	
	@Test
	public void oneChangeVariationsTest1() { 
		ArrayList<Boolean> input = new ArrayList<>(Arrays.asList(false, false, false));

		ArrayList<ArrayList<Boolean>> expect = new ArrayList<>();
		expect.add(new ArrayList<Boolean>(Arrays.asList(true, false, false)));
		expect.add(new ArrayList<Boolean>(Arrays.asList(false, true, false)));
		expect.add(new ArrayList<Boolean>(Arrays.asList(false, false, true)));
		assertEquals(expect, ParamsUtils.getOneChangeVariations(input));
		
	}
}

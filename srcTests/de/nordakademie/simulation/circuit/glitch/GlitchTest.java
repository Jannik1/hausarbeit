package de.nordakademie.simulation.circuit.glitch;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import de.nordakademie.simulation.circuit.hazard.Glitch;

/**
 * Test the {@link Glitch}.
 */
public class GlitchTest {

    /**
     * Class under test.
     */
    private Glitch.Builder gBuilder;

    /**
     * Sets up a {@link Glitch} for tests.
     */
    @Before
    public void setUp() {
        gBuilder = new Glitch.Builder();
        List<Boolean> from = new ArrayList<>(Collections.singletonList(true));
        List<Boolean> to = new ArrayList<>(Collections.singletonList(true));

        gBuilder
                .withFromInput(from)
                .withToInput(to);
    }

    /**
     * Tests the toString implementation of {@link Glitch}.
     */
    @Test
    public void testGlitchToString() {
        //noinspection OptionalGetWithoutIsPresent
        assertEquals(
                "Glitch found from config: [true] -> [true]",
                gBuilder.forResult(Arrays.asList(true, false, true)).build().get().toString());
    }

    /**
     * Tests the toString implementation of {@link Glitch}.
     */
    @Test
    public void testNoGlitch() {
        assertEquals(Optional.empty(), gBuilder.forResult(Arrays.asList(true, true)).build());
    }
}

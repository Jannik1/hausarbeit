package de.nordakademie.simulation.circuit.gate.impl;

import de.nordakademie.simulation.circuit.gate.Gate;

/**
 * An implementation of {@link Gate} for a logical NOT.
 */
public class Not extends Gate {

    @Override
    protected boolean out(final boolean... in) {
        if (in.length == 0) {
            throw new IllegalArgumentException();
        }
        return !in[0];
    }

    @Override
    public int getDelay() {
        return 1;
    }
}

package de.nordakademie.simulation.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides a few methods to create input-parameters.
 */
public final class ParamUtils {

    /**
     * Empty-Constructor to avoid unnecessary instances.
     */
    private ParamUtils() {
    }

    /**
     * This Method creates all boolean permutations with given length.
     *
     * @param length of permutation
     * @return s the result inside a list of lists (with the given length)
     */
    public static List<List<Boolean>> variationsWithRepetition(final int length) {
        return variationsWithRepetition(new ArrayList<>(), new ArrayList<>(), length);
    }

    /**
     * A recursive permutation method.
     *
     * @param permutations an accumulating list of all permutations.
     * @param seed    list used to recursively create all permutations.
     * @param length  required length of each permutation.
     * @return a list of all permutations with given length.
     */
    private static List<List<Boolean>> variationsWithRepetition(
            final List<List<Boolean>> permutations,
            final List<Boolean> seed,
            final int length) {
        if (seed.size() >= length) {
            permutations.add(seed);
        } else {
            List<Boolean> seedTrue = new ArrayList<>(seed);
            seedTrue.add(true);
            variationsWithRepetition(permutations, seedTrue, length);

            List<Boolean> seedFalse = new ArrayList<>(seed);
            seedFalse.add(false);
            variationsWithRepetition(permutations, seedFalse, length);
        }
        return permutations;
    }

    /**
     * This Method return a list of boolean combinations that differ by one
     * change to a given input.
     *
     * @param input (list of booleans)
     * @return a list of booleans that differ by one against the input.
     */
    public static List<List<Boolean>> variationNoRepetition(
            final List<Boolean> input) {
        List<List<Boolean>> oneChangeVariations = new ArrayList<>();

        for (int index = 0; index < input.size(); index++) {
            List<Boolean> copiedInput = new ArrayList<>(input);
            copiedInput.set(index, !copiedInput.get(index));
            oneChangeVariations.add(copiedInput);
        }

        return oneChangeVariations;
    }
}

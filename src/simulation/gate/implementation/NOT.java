package simulation.gate.implementation;

import simulation.gate.Gate;

public class NOT extends Gate {
	
	public NOT(boolean...in) {
		super(in);
	}

	@Override
	protected boolean out(boolean... in) {
		return !in[0];
	}

	@Override
	public int getDelay() {
		return 1;
	}
	
	@Override
	protected int getInputCount() {
		return 1;
	}
}

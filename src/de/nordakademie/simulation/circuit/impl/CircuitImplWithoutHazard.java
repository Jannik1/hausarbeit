package de.nordakademie.simulation.circuit.impl;

import java.util.Arrays;
import java.util.List;

import de.nordakademie.simulation.circuit.Circuit;
import de.nordakademie.simulation.circuit.gate.GateFactory;
import de.nordakademie.simulation.circuit.gate.IGate;

public class CircuitImplWithoutHazard extends Circuit{
    /**
     * {@link IGate}'s for the implemented {@link Circuit}.
     */
    private IGate and;

    /**
     * Constructor.
     */
    public CircuitImplWithoutHazard() {
        GateFactory gateFactory = new GateFactory();
        and = gateFactory.getAnd();
     
    }

    @Override
    public final int getInputCount() {
        return 2;
    }

    @Override
    protected final boolean run(final List<Boolean> input) {
        return and.f(input.get(0), input.get(1));
    }

    @Override
    protected final List<IGate> getGates() {
        return Arrays.asList(and);
    }

    @Override
    public final String toString() {
        return and.toString("x0", "x1");
    }

}

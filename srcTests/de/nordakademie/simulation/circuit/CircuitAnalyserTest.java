package de.nordakademie.simulation.circuit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import de.nordakademie.simulation.circuit.hazard.Glitch;

/**
 * Tests {@link CircuitAnalyser},
 */
public class CircuitAnalyserTest {

    /**
     * Class under test.
     */
    private CircuitAnalyser analyser;
    private ICircuit circuit;

    @Before
    public void setup() {
        circuit = mock(ICircuit.class);
    }

    @Test
    public void testAnalyseWithoutAnInput(){
    	 when(circuit.getInputCount()).thenReturn(0);
    	 when(circuit.execute(any())).thenReturn(new ArrayList<>());
    	 analyser = new CircuitAnalyser(circuit);
    	 List<String> glitches =  analyser.analyse().stream().map(Glitch::toString).collect(Collectors.toList());
    	 assertTrue(glitches.isEmpty());
    }
    
    @Test
    public void testAnalyseCircuitExecuteReturnsNull(){
    	 when(circuit.getInputCount()).thenReturn(0);
    	 when(circuit.execute(any())).thenReturn(null);
    	 analyser = new CircuitAnalyser(circuit);
    	 List<String> glitches =  analyser.analyse().stream().map(Glitch::toString).collect(Collectors.toList());
    	 assertTrue(glitches.isEmpty());
    }
    
    @Test
    public void testAnalyseWithHazardForOneInput() {
        when(circuit.getInputCount()).thenReturn(1);
        when(circuit.execute(any())).thenReturn(Arrays.asList(true, false, true));
        analyser = new CircuitAnalyser(circuit);
        
        List<String> glitches =  analyser.analyse().stream().map(Glitch::toString).collect(Collectors.toList());
        assertEquals(Arrays.asList(
                "Glitch found from config: [true] -> [false]",
                "Glitch found from config: [false] -> [true]"), glitches);
    }
    
    @Test
    public void testAnalyseWithoutHazardForOneInput() {
        when(circuit.getInputCount()).thenReturn(1);
        when(circuit.execute(any())).thenReturn(Arrays.asList(true, false, false));
        analyser = new CircuitAnalyser(circuit);
        
        List<String> glitches =  analyser.analyse().stream().map(Glitch::toString).collect(Collectors.toList());
        assertTrue(glitches.isEmpty());
    }
    
    @Test
    public void testAnalyseWithTwoHazardsForOneInput() {
        when(circuit.getInputCount()).thenReturn(1);
        when(circuit.execute(any())).thenReturn(Arrays.asList(true, false, true, false));
        analyser = new CircuitAnalyser(circuit);
        
        List<String> glitches =  analyser.analyse().stream().map(Glitch::toString).collect(Collectors.toList());
        assertEquals(Arrays.asList(
                "Glitch found from config: [true] -> [false]",
                "Glitch found from config: [false] -> [true]"), glitches);
    }
    
    
    @Test
    public void testAnalyseWithoutHazardForTwoInputs() {
         when(circuit.getInputCount()).thenReturn(2);
         when(circuit.execute(any())).thenReturn(Arrays.asList(true, false, false));

         analyser = new CircuitAnalyser(circuit);
         List<String> glitches =  analyser.analyse().stream().map(Glitch::toString).collect(Collectors.toList());
         assertTrue(glitches.isEmpty());
    	
    }
    
    
    @Test
    public void testAnalyseWithHazardForTwoInputs() {
         when(circuit.getInputCount()).thenReturn(2);
         when(circuit.execute(any())).thenReturn(Arrays.asList(true, false, true));

         analyser = new CircuitAnalyser(circuit);
         List<String> glitches =  analyser.analyse().stream().map(Glitch::toString).collect(Collectors.toList());
         assertEquals(Arrays.asList(
        		 "Glitch found from config: [true, true] -> [false, true]",
        		 "Glitch found from config: [true, true] -> [true, false]", 
        		 "Glitch found from config: [true, false] -> [false, false]",
        		 "Glitch found from config: [true, false] -> [true, true]",
        		 "Glitch found from config: [false, true] -> [true, true]",
        		 "Glitch found from config: [false, true] -> [false, false]",
        		 "Glitch found from config: [false, false] -> [true, false]",
        		 "Glitch found from config: [false, false] -> [false, true]"), glitches);
    	
    }

}

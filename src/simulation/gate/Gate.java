package simulation.gate;

import java.util.ArrayList;

import simulation.gate.exception.InvalidDelayException;
import simulation.gate.exception.InvalidInputException;

/**
 * This is a abstract logic gate for a boolean function. It behaves after a
 * certain delay-behavior.
 * 
 * @author Finn von Holten
 *
 */
public abstract class Gate implements IGate {

	private ArrayList<Boolean> outStack = new ArrayList<>();

	/**
	 * Constructor
	 * 
	 * @param in (the initial input for a gate)
	 */
	public Gate(boolean... in) {
		checkInput(in);
		outStack.add(out(in));
	}

	private void checkInput(boolean... in) {
		if (in == null || in.length != getInputCount()) {
			throw new InvalidInputException();
		}
		if (getDelay() < 0) {
			throw new InvalidDelayException();
		}
	}

	@Override
	public boolean f(boolean... in) {
		checkInput(in);
		boolean out = out(in);
		if (getDelay() == 0) {
			return out;
		} else {
			outStack.add(out);
			if ((outStack.size() - 1) == getDelay()) {
				return outStack.remove(0);
			} else {
				return outStack.get(0);
			}
		}
	}

	public String toString(String... str) {
		StringBuilder result = new StringBuilder();
		result.append(this.getClass().getSimpleName() + "(");
		result.append(String.join(", ", str));
		result.append(")");
		return result.toString();
	}

	/**
	 * This Method is the function to calculate the result of a gate with the
	 * given input. It has to be implemented.
	 * 
	 * @param in
	 *            (the input variables)
	 * @return (the result of the gate with given input)
	 */
	protected abstract boolean out(boolean... in);

	protected abstract int getInputCount();
}
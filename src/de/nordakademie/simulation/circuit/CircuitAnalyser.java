package de.nordakademie.simulation.circuit;

import de.nordakademie.simulation.circuit.hazard.Glitch;
import de.nordakademie.simulation.utils.ParamUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Analyses {@link ICircuit} for hazards.
 */
public class CircuitAnalyser {

    /**
     * Circuit to be analysed.
     */
    private ICircuit circuit;

    /**
     * Constructor.
     *
     * @param iCircuit {@link ICircuit} to be analysed
     */
    public CircuitAnalyser(final ICircuit iCircuit) {
        this.circuit = iCircuit;
    }

    /**
     * Analyse all possible from -> to combinations of a circuit for
     * possible hazards.
     *
     * @return a list of all glitches found in the circuit
     */
    public final List<Glitch> analyse() {
        Glitch.Builder glitchBuilder = new Glitch.Builder();
        List<Glitch> protocol = new ArrayList<>();
        ParamUtils.variationsWithRepetition(circuit.getInputCount()).forEach(input ->
                ParamUtils.variationNoRepetition(input).forEach(to -> {
                            circuit.execute(input);
                            glitchBuilder
                                    .withFromInput(input)
                                    .withToInput(to)
                                    .forResult(circuit.execute(to))
                                    .build()
                                    .ifPresent(protocol::add);
                        }

                )
        );
        return protocol;
    }
}

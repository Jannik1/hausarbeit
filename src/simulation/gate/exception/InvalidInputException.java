package simulation.gate.exception;

/**
 * 
 * This Exception is thrown when an input for a function is invalid.
 * 
 * @author xxx
 *
 */
@SuppressWarnings("serial")
public class InvalidInputException extends RuntimeException {

}

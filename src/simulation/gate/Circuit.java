package simulation.gate;

import java.util.ArrayList;

import simulation.gate.exception.InvalidInputException;
import simulation.gate.implementation.AND;
import simulation.gate.implementation.NOT;
import simulation.gate.implementation.OR;
import simulation.utils.CircuitUtils;

/**
 * Ein Circuit ist ein fest-programmierter Schaltkreis.
 * 
 * @author finn
 *
 */
public class Circuit {
	
	private NOT not;
	private OR or;
	private AND and, and2;
	private ArrayList<Boolean> from;
	
	private final static int INPUT_COUNT = 3;
	
	/**
	 * @param input (die Standardbelegung der Schaltung)
	 */
	public Circuit(ArrayList<Boolean> input) {
		checkInput(input);
		this.from = input;
	}
	
	private void checkInput(ArrayList<Boolean> input) {
		if (input.size() != INPUT_COUNT) {
			throw new InvalidInputException();
		}
	}
	
	/**
	 * @return s the number of inputs the circuit has to receive.
	 */
	public static int getInputCount() {
		return INPUT_COUNT;
	}
	
	/**
	 * Executes the circuit with a given
	 * @param input unit it reaches its final state
	 * @return all states in with the circuit was unit it reached a final state
	 */
	public ArrayList<Boolean> execute(ArrayList<Boolean> input) {
		checkInput(input);
		
		not = new NOT(this.from.get(1));
		and = new AND(this.from.get(0), this.from.get(1));
		and2 = new AND(this.from.get(2), !this.from.get(1));
		or = new OR(this.from.get(2) && !this.from.get(1), this.from.get(0) && this.from.get(1));
				
		ArrayList<Boolean> resultStack = new ArrayList<>();
		
		for (int now = 0; now < CircuitUtils.maxDuration(not, and, and2, or); now++) {
			resultStack.add(or.f(and2.f(input.get(2), not.f(input.get(1))), and.f(input.get(0), input.get(1))));
		}
		
		return resultStack;
	}
	
	@Override
	public String toString() {
		return or.toString(and2.toString("x2", not.toString("x1")), and.toString("x0", "x1"));
	}
}

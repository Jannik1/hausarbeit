package simulation.gate.exception;

/**
 * This Exception is thrown is a gate delay-behavior is invalid.
 * 
 * @author finn
 *
 */
@SuppressWarnings("serial")
public class InvalidDelayException extends RuntimeException {

}

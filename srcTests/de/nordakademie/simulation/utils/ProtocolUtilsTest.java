package de.nordakademie.simulation.utils;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 * Tests {@link ProtocolUtils}.
 */
public class ProtocolUtilsTest {

    /**
     * Tests the correct functionality of protocol.
     *
     * @throws IOException (due to I/O operations)
     */
    @Test
    public void testProtocol() throws IOException {
        List<String> protocol = new ArrayList<>(Arrays.asList("1", "2"));

        ProtocolUtils.writeProtocol(protocol, "output");

        BufferedReader br = new BufferedReader(new FileReader("output.txt"));

        assertEquals(
                br.lines().collect(Collectors.toList()).toString(),
                protocol.toString());

        br.close();
    }


}

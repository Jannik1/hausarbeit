package de.nordakademie.simulation.circuit.gate;

import java.util.ArrayList;
import java.util.List;

/**
 * This is a abstract logic gate for a boolean function. It behaves after a
 * certain delay-behavior.
 */
public abstract class Gate implements IGate {

    /**
     * This variable represents the current result
     * stack of a {@link Gate} to enable a delay behavior.
     */
    private List<Boolean> outStack = new ArrayList<>();

    /**
     * This Method checks the input of the {@link Gate} for validity.
     *
     * @param in input that should be verified.
     */
    private void checkInput(final boolean... in) {
        if (in == null || getDelay() < 0) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public final boolean f(final boolean... in) {
        checkInput(in);
        outStack.add(out(in));
        if ((outStack.size() - 1) == getDelay()) {
            return outStack.remove(0);
        } else {
            return outStack.get(0);
        }
    }

    @Override
    public final String toString(final String... str) {
        return this.getClass().getSimpleName() + "("
                + String.join(", ", str) + ")";
    }

    /**
     * This method calculates the result of a gate with a
     * given input. It has to be implemented.
     *
     * @param in the input variables
     * @return the result of the gate with given input
     */
    protected abstract boolean out(boolean... in);
}

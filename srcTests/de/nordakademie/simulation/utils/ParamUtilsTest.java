package de.nordakademie.simulation.utils;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Tests {@link ParamUtils}.
 */
public class ParamUtilsTest {

    /**
     * tests permutation for correct outputs.
     */
    @Test
    public void testVariationsWithRepetitionLength() {
        int length = -1;
        assertEquals(1, ParamUtils.variationsWithRepetition(length).size());
        length = 0;
        assertEquals(1, ParamUtils.variationsWithRepetition(length).size());
        length = 1;
        assertEquals((int) Math.pow(2, length),
                ParamUtils.variationsWithRepetition(length).size());
        length = 2;
        assertEquals((int) Math.pow(2, length),
                ParamUtils.variationsWithRepetition(length).size());
        length = 3;
        assertEquals((int) Math.pow(2, length),
                ParamUtils.variationsWithRepetition(length).size());
        length = 4;
        assertEquals((int) Math.pow(2, length),
                ParamUtils.variationsWithRepetition(length).size());
    }

    /**
     * test oneChangeVariations.
     */
    @Test
    public void testVariationNoRepetition() {
        List<Boolean> input = new ArrayList<>(Arrays.asList(true, true, true));
        List<List<Boolean>> expect = new ArrayList<>();
        expect.add(new ArrayList<>(Arrays.asList(false, true, true)));
        expect.add(new ArrayList<>(Arrays.asList(true, false, true)));
        expect.add(new ArrayList<>(Arrays.asList(true, true, false)));
        assertEquals(expect, ParamUtils.variationNoRepetition(input));

    }
}

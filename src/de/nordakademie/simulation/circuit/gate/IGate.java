package de.nordakademie.simulation.circuit.gate;

/**
 * An IGate has a single function f which return the result of a gate.
 */
public interface IGate {

    /**
     * This Method contains the default behavior of any Gate.
     *
     * @param in (the parameters for which a result should be calculated)
     * @return the result of a gate
     */
    boolean f(boolean... in);

    /**
     * @return s the delay of a gate.
     */
    int getDelay();

    /**
     * Custom toString method to print an {@link IGate} properly.
     *
     * @param str input params for the gate.
     * @return a human readable {@link IGate} string.
     */
    String toString(String... str);
}

package de.nordakademie.simulation.circuit.gate;

import de.nordakademie.simulation.circuit.gate.impl.And;
import de.nordakademie.simulation.circuit.gate.impl.Not;
import de.nordakademie.simulation.circuit.gate.impl.Or;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Tests {@link GateFactory}.
 */
public class GateFactoryTest {

    /**
     * Class under test.
     */
    private GateFactory factory;

    /**
     * sets up a {@link GateFactory}.
     */
    @Before
    public void setup() {
        factory = new GateFactory();
    }

    /**
     * Tests fabrication of {@link And}.
     */
    @Test
    public void testAndFactory() {
        assertTrue(factory.getAnd() instanceof And);
    }

    /**
     * Tests fabrication of {@link Or}.
     */
    @Test
    public void testOrFactory() {
        assertTrue(factory.getOr() instanceof Or);
    }

    /**
     * Tests fabrication of {@link Not}.
     */
    @Test
    public void testNotFactory() {
        assertTrue(factory.getNot() instanceof Not);
    }
}

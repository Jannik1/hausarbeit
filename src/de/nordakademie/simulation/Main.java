package de.nordakademie.simulation;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import de.nordakademie.simulation.circuit.CircuitAnalyser;
import de.nordakademie.simulation.circuit.ICircuit;
import de.nordakademie.simulation.circuit.hazard.Glitch;
import de.nordakademie.simulation.circuit.impl.CircuitImplWithHazard;
import de.nordakademie.simulation.utils.ProtocolUtils;

/**
 * Main class of the project.
 */
public final class Main {

    /**
     * Empty-Constructor to avoid unnecessary instances.
     */
    private Main() {

    }

    /**
     * main method to start the app.
     *
     * @param args not needed.
     * @throws IOException (because of I/O operations.)
     */
    public static void main(final String... args) throws IOException {

        ICircuit circuit = new CircuitImplWithHazard();
        CircuitAnalyser circuitAnalyser = new CircuitAnalyser(circuit);

        List<Glitch> glitches = circuitAnalyser.analyse();
        List<String> log = glitches.stream()
                .map(Glitch::toString)
                .collect(Collectors.toList());

        ProtocolUtils.writeProtocol(log, circuit.toString());
    }
}

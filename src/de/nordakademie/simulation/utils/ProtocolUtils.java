package de.nordakademie.simulation.utils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Provides protocol Utilities.
 */
public final class ProtocolUtils {


    /**
     * Empty-Constructor to avoid unnecessary instances.
     */
    private ProtocolUtils() {
    }

    /**
     * This Method creates a protocol-file called
     * "circuitName.txt". If an exception is thrown the protocol
     * will be printed to the console.
     *
     * @param results     this is what the method writes into the
     *                    "output.txt" file.
     * @param circuitName the name of the circuit,
     *                    will be used as output filename.
     * @throws IOException (due to I/O operations)
     */
    public static void writeProtocol(
            final List<String> results, final String circuitName)
            throws IOException {
        Path out = Paths.get(circuitName + ".txt");
        Files.write(out, results, Charset.defaultCharset());
    }
}

package simulation.utils;

import java.util.ArrayList;

/**
 * 
 * @author finn
 *
 */
public class HazardDetector {

	/**
	 * This Method detects glitches in a
	 * @param resultStack (a list of all results a circuit created until it reached its final state.)
	 * @return if there is a glitch in the resultStack. 
	 */
	public static boolean isGlitched(ArrayList<Boolean> resultStack) {
		int changes = 0;
		if (!resultStack.isEmpty()) {
			boolean predesessor = resultStack.get(0);
			for (boolean b : resultStack) {
				if (predesessor != b) {
					changes++;
					predesessor = b;
				}
			}
		}
		return changes > 1;
	}
}
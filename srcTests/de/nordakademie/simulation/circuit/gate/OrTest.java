package de.nordakademie.simulation.circuit.gate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.nordakademie.simulation.circuit.gate.impl.Or;

/**
 * Tests {@link Or}.
 */
public class OrTest {


    private IGate logicalOr;

    @Before
    public void setup() {
        GateFactory gateFactory = new GateFactory();
        logicalOr = spy(gateFactory.getOr());
    }


    /**
     * Tests invalid {@link Or}.
     */
    @Test
    public void testOrWithoutInput() {
        assertFalse(logicalOr.f());
    }

    /**
     * Test the functionality of {@link Or}.
     */
    @Test
    public void testOrValidWithDelay() {
        when(logicalOr.getDelay()).thenReturn(2);
        logicalOr.f(false, false);

        assertFalse(logicalOr.f(true, true));
        assertFalse(logicalOr.f(false, true));
        assertTrue(logicalOr.f(true, true));
        assertTrue(logicalOr.f(false, true));
    }

    /**
     * Test the functionality of {@link Or}.
     */
    @Test
    public void testOrValidNoDelay() {
        when(logicalOr.getDelay()).thenReturn(0);
        logicalOr.f(false, false);

        assertTrue(logicalOr.f(true, true));
        assertTrue(logicalOr.f(false, true));
        assertTrue(logicalOr.f(true, true));
        assertFalse(logicalOr.f(false, false));
    }

    /**
     * Test the functionality of {@link Or}.
     */
    @Test
    public void test3OrValidWithDelay() {
        when(logicalOr.getDelay()).thenReturn(2);
        logicalOr.f(false, false, false);

        assertFalse(logicalOr.f(true, true, false));
        assertFalse(logicalOr.f(false, true));
        assertTrue(logicalOr.f(true, true));
        assertTrue(logicalOr.f(false, true));
    }

    /**
     * Tests the intended functionality of toString.
     */
    @Test
    public void testToString() {
    	GateFactory gateFactory = new GateFactory();
    	IGate or = gateFactory.getOr();
        Assert.assertEquals("Or(x1, x2, x3)", or.toString("x1", "x2", "x3"));
        Assert.assertEquals("Or(x1, x2)", or.toString("x1", "x2"));
        Assert.assertEquals("Or(x1)", or.toString("x1"));
    }
}

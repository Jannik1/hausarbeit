package de.nordakademie.simulation.circuit.impl;

import de.nordakademie.simulation.circuit.Circuit;
import de.nordakademie.simulation.circuit.gate.GateFactory;
import de.nordakademie.simulation.circuit.gate.IGate;

import java.util.Arrays;
import java.util.List;

/**
 * CircuitImpl is a hardcoded circuit implementation of {@link Circuit}.
 */
public class CircuitImplWithHazard extends Circuit {

    /**
     * {@link IGate}'s for the implemented {@link Circuit}.
     */
    private IGate not, or, and, and2;

    /**
     * Constructor.
     */
    public CircuitImplWithHazard() {
        GateFactory gateFactory = new GateFactory();
        not = gateFactory.getNot();
        or = gateFactory.getOr();
        and = gateFactory.getAnd();
        and2 = gateFactory.getAnd();
    }

    @Override
    public final int getInputCount() {
        return 3;
    }

    @Override
    protected final boolean run(final List<Boolean> input) {
        return or.f(
                and2.f(input.get(2), not.f(input.get(1))),
                and.f(input.get(0), input.get(1)));
    }

    @Override
    protected final List<IGate> getGates() {
        return Arrays.asList(and, or, and2, not);
    }

    @Override
    public final String toString() {
        return or.toString(
                and2.toString("x2", not.toString("x1")),
                and.toString("x0", "x1"));
    }
}

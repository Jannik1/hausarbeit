package de.nordakademie.simulation.circuit.impl;

import de.nordakademie.simulation.circuit.Circuit;
import de.nordakademie.simulation.circuit.ICircuit;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * This is test the circuit implementation of {@link Circuit}.
 */
public class CircuitImplWithHazardTest {

    /**
     * Class under test (CUT).
     */
    private ICircuit circuit;

    /**
     * creates a new {@link CircuitImpl} as a setup measure.
     */
    @Before
    public void setUp() {
        circuit = new CircuitImplWithHazard();
    }

    /**
     * tests the correct implementation of toString.
     */
    @Test
    public void testToString() {
        assertEquals("Or(And(x2, Not(x1)), And(x0, x1))", circuit.toString());
    }

    /**
     * tests execute of {@link CircuitImpl}
     */
    @Test
    public void testExecute() {
        circuit.execute(Arrays.asList(true, true, true));
        assertEquals(
                Arrays.asList(true, true, true, true, false, true, true),
                circuit.execute(Arrays.asList(true, false, true)));
    }

    /**
     * tests execute of {@link CircuitImpl}
     */
    @Test(expected = IllegalArgumentException.class)
    public void testINVALIDExecute() {
        circuit.execute(Arrays.asList(false, true));
    }

    @Test
    public void testGetInputCount() {
        assertEquals(3, circuit.getInputCount());
    }
}

package gate_test.implementation_test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import simulation.gate.exception.InvalidInputException;
import simulation.gate.implementation.AND;

public class ANDTest {
	

	@Test(expected = InvalidInputException.class)
	public void exceptionWithoutParamANDTest() {
		new AND();
	}
	
	@Test(expected = InvalidInputException.class)
	public void exceptionWithOneParamANDTest() {
		new AND(true);
	}

	@Test
	public void validANDTest() {
		AND logicAnd = new AND(false, true);
		
		assertEquals(false, logicAnd.f(true, true));
		assertEquals(false, logicAnd.f(false, true));
		assertEquals(true, logicAnd.f(true, true));
		assertEquals(false, logicAnd.f(false, true));
	}
	
	@Test(expected = InvalidInputException.class)
	public void exceptionWithThreeParamANDTest() {
		new AND(true, false, true);
	}

}

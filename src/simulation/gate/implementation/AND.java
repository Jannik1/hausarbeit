package simulation.gate.implementation;

import simulation.gate.Gate;

public class AND extends Gate {

	public AND(boolean... in) {
		super(in);
	}

	@Override
	protected boolean out(boolean... in) {
		return in[0] && in[1];
	}

	@Override
	public int getDelay() {
		return 2;
	}

	@Override
	protected int getInputCount() {
		return 2;
	}
}

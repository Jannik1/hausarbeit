package simulation.utils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class FileUtils {

	public static void writeFile(ArrayList<String> execute) {
		Path out = Paths.get("output.txt");
		try {
			Files.write(out,execute,Charset.defaultCharset());
		} catch (IOException e) {
			System.err.println("Die Datei output.txt konnte nicht erzeugt werden.");
			e.printStackTrace();
		}
	}
}

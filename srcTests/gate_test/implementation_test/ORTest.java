package gate_test.implementation_test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import simulation.gate.exception.InvalidInputException;
import simulation.gate.implementation.OR;

public class ORTest {
	

	@Test(expected = InvalidInputException.class)
	public void exceptionWithoutParamORTest() {
		new OR();
	}
	
	@Test(expected = InvalidInputException.class)
	public void exceptionWithOneParamORTest() {
		new OR(true);
	}

	@Test
	public void validORTest() {
		OR logicOR = new OR(false, true);
		
		assertEquals(false || true, logicOR.f(true, true));
		assertEquals(false || true, logicOR.f(false, true));
		assertEquals(true, logicOR.f(true, true));
		assertEquals(false || true, logicOR.f(false, true));
	}
	
	@Test(expected = InvalidInputException.class)
	public void exceptionWithThreeParamORTest() {
		new OR(true, false, true);
	}

}

package gate_test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import simulation.gate.Gate;
import simulation.gate.exception.InvalidDelayException;

public class GateTest {

	private Gate gate;

	@Test
	public void testGateDelayZero() {
		this.gate = new Gate(true) {

			@Override
			protected boolean out(boolean... in) {
				return in[0];
			}

			@Override
			public int getDelay() {
				return 0;
			}
			
			@Override
			protected int getInputCount() {
				return 1;
			}
		};
		assertEquals(false, this.gate.f(false));
		assertEquals(true, this.gate.f(true));
	}
	
	@Test
	public void testGateDelayOne() {
		this.gate = new Gate(true) {

			@Override
			protected boolean out(boolean... in) {
				return in[0];
			}

			@Override
			public int getDelay() {
				return 1;
			}
			@Override
			protected int getInputCount() {
				return 1;
			}
		};
		assertEquals(true, this.gate.f(false));
		assertEquals(false, this.gate.f(true));
	}
	
	@Test(expected = InvalidDelayException.class)
	public void testGateInvalidDelay() {
		new Gate(true) {
			
			@Override
			protected boolean out(boolean... in) {
				return false;
			}
			
			@Override
			public int getDelay() {
				return -1;
			}
			@Override
			protected int getInputCount() {
				return 1;
			}
		};
	}

}

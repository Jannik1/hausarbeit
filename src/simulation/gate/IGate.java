package simulation.gate;

/**
 * 
 * An IGate has a single function f which return the result of a gate.
 * 
 * @author finn
 *
 */
public interface IGate {

	/**
	 * This Method contains the default behavior of any Gate.
	 * 
	 * @param in (the parameters for which a result should be calculated)
	 * @return the result of a gate
	 */
	boolean f(boolean... in);

	/**
	 * @return s the delay of a gate.
	 */
	abstract int getDelay();
}

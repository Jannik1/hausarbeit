package de.nordakademie.simulation.circuit;

import de.nordakademie.simulation.circuit.gate.IGate;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract logic circuit.
 */
public abstract class Circuit implements ICircuit {

    @Override
    public final List<Boolean> execute(final List<Boolean> input) {
        if (input.size() != getInputCount()) {
            throw new IllegalArgumentException();
        }
        // get results
        List<Boolean> results = new ArrayList<>();
        for (int now = 0; now < maxDuration(getGates()); now++) {
            results.add(run(input));
        }
        return results;
    }

    /**
     * Method to run a circuit once.
     *
     * @param input the input for the circuit to run with.
     * @return the result of this circuit execution/run.
     */
    protected abstract boolean run(List<Boolean> input);

    /**
     * Method to get all Gates used in a circuit.
     *
     * @return a list of all gates in use.
     */
    protected abstract List<IGate> getGates();

    /**
     * Calculates the maximum time a {@link Circuit} takes to reach its final state.
     *
     * @param gates the gates which given circuit contains.
     * @return the maximum duration a circuit needs to reach its final state.
     */
    private int maxDuration(final List<IGate> gates) {
        return gates.stream().mapToInt(IGate::getDelay).sum();
    }
}

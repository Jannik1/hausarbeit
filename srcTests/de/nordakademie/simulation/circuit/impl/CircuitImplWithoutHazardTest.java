package de.nordakademie.simulation.circuit.impl;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import de.nordakademie.simulation.circuit.ICircuit;

public class CircuitImplWithoutHazardTest {

    /**
     * Class under test (CUT).
     */
    private ICircuit circuit;

    /**
     * creates a new {@link CircuitImpl} as a setup measure.
     */
    @Before
    public void setUp() {
        circuit = new CircuitImplWithoutHazard();
    }

    /**
     * tests the correct implementation of toString.
     */
    @Test
    public void testToString() {
        assertEquals("And(x0, x1)", circuit.toString());
    }

    /**
     * tests execute of {@link CircuitImpl}
     */
    @Test
    public void testExecute() {
        circuit.execute(Arrays.asList(true, true));
        assertEquals(
                Arrays.asList(true, true),
                circuit.execute(Arrays.asList(true, true)));
    }

    /**
     * tests execute of {@link CircuitImpl}
     */
    @Test(expected = IllegalArgumentException.class)
    public void testINVALIDExecute() {
        circuit.execute(Arrays.asList(false));
    }

    @Test
    public void testGetInputCount() {
        assertEquals(2, circuit.getInputCount());
    }
}

package de.nordakademie.simulation.circuit.gate;

import de.nordakademie.simulation.circuit.gate.impl.And;
import de.nordakademie.simulation.circuit.gate.impl.Not;
import de.nordakademie.simulation.circuit.gate.impl.Or;


/**
 * Factory for {@link IGate}.
 */
public class GateFactory {
	
	 /**
     * Fabricates an {@link And}.
     * @return an {@link IGate} as {@link And}.
     */
    public IGate getAnd() {
        return new And();
    }
    /**
     * Fabricates an {@link Or}.
     * @return an {@link IGate} as {@link Or}.
     */
    public IGate getOr() {
    	return new Or();
    }
    /**
     * Fabricates an {@link Not}.
     * @return an {@link IGate} as {@link Not}.
     */
    public IGate getNot() {
    	return new Not();
    }

}

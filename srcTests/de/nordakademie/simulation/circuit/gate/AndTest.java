package de.nordakademie.simulation.circuit.gate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.nordakademie.simulation.circuit.gate.impl.And;
import de.nordakademie.simulation.circuit.gate.impl.Or;

/**
 * Test for the logic {@link And} implementation.
 */
public class AndTest {

	
	private IGate logicalAnd;
	
    @Before
    public void setup() {
    	GateFactory gateFactory = new GateFactory();
    	logicalAnd = spy(gateFactory.getAnd());
    }


    /**
     * This test tests a valid {@link And} configuration for its
     * functional implementation.
     */
    @Test
    public void testAndWithoutInput() {
        assertFalse(logicalAnd.f());
    }
    
    
    /**
     * Tests invalid {@link Or}.
     */
    @Test(expected = IllegalArgumentException.class) 
    public void testNegativeDelay() {
    	when(logicalAnd.getDelay()).thenReturn(-1);
    	logicalAnd.f(true);
    }

    /**
     * Tests invalid {@link Or}.
     */
    @Test(expected = IllegalArgumentException.class) 
    public void testNull() {
    	logicalAnd.f(null);
    }
    
    
    
    /**
     * This test tests a valid {@link And} configuration for its
     * functional implementation.
     */
    @Test
    public void testAndWithoutDelay() {
        when(logicalAnd.getDelay()).thenReturn(0);

        assertTrue(logicalAnd.f(true));
        assertFalse(logicalAnd.f(false));
    }

    /**
     * This test tests a valid {@link And} configuration for its
     * functional implementation.
     */
    @Test
    public void testAndWithOneDelay() {
        when(logicalAnd.getDelay()).thenReturn(1);
        
        logicalAnd.f(true);
        assertTrue(logicalAnd.f(false));
        assertFalse(logicalAnd.f(true));
    }
    
    /**
     * This test tests a valid {@link And} configuration for its
     * functional implementation.
     */
    @Test
    public void testAndWithTwoInputs() {
        when(logicalAnd.getDelay()).thenReturn(2);
        logicalAnd.f(false, true);

        assertFalse(logicalAnd.f(true, true));
        assertFalse(logicalAnd.f(false, true));
        assertTrue(logicalAnd.f(true, true));
        assertFalse(logicalAnd.f(false, true));
    }
    
    /**
     * This test tests a valid {@link And} configuration for its
     * functional implementation.
     */
    @Test
    public void testAndWithTwoInputsWithoutDelay() {
        when(logicalAnd.getDelay()).thenReturn(0);

        assertTrue(logicalAnd.f(true, true));
        assertFalse(logicalAnd.f(false, true));
        assertFalse(logicalAnd.f(false, false));
        assertFalse(logicalAnd.f(true, false));
    }
    
    
    /**
     * This test tests a valid {@link And} configuration for its
     * functional implementation.
     */
    @Test
    public void testAndWithThreeInputs() {
        when(logicalAnd.getDelay()).thenReturn(2);
        logicalAnd.f(false, true, true);

        assertFalse(logicalAnd.f(true, true, true));
        assertFalse(logicalAnd.f(false, true, true));
        assertTrue(logicalAnd.f(true, true, false));
        assertFalse(logicalAnd.f(false, true, false));
    }

    /**
     * This test tests if toString works as intended.
     */
    @Test
    public void testToString() {
    	GateFactory gateFactory = new GateFactory();
    	IGate and = gateFactory.getAnd();
        Assert.assertEquals("And(x1, x2, x3)", and.toString("x1", "x2", "x3"));
        Assert.assertEquals("And(x1, x2)", and.toString("x1", "x2"));
        Assert.assertEquals("And(x1)", and.toString("x1"));
    }

}

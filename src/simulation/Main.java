package simulation;
import java.util.ArrayList;

import simulation.gate.CircuitExecuter;
import simulation.utils.FileUtils;

/**
 * 
 * @author Jannik
 *
 */

public class Main {
	
	public static void main(String[] args) {
		ArrayList<String> execute = new CircuitExecuter().execute();
		FileUtils.writeFile(execute);
	}
}

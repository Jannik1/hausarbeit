package de.nordakademie.simulation.circuit.gate;

import de.nordakademie.simulation.circuit.gate.impl.Not;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

/**
 * Test the implementation of {@link Not}.
 */
public class NotTest {

    private IGate logicalNot;

    @Before
    public void setup() {
        GateFactory gateFactory = new GateFactory();
        logicalNot = spy(gateFactory.getNot());
    }

    /**
     * Test a {@link Not} with illegal params (no params).
     */
    @Test(expected = IllegalArgumentException.class)
    public void testNotExceptionNoParams() {
    	logicalNot.f();
    }

    /**
     * Test the valid {@link Not} logic.
     */
    @Test
    public void testNotValidWithDelay() {
        when(logicalNot.getDelay()).thenReturn(1);
        logicalNot.f(false);

        assertTrue(logicalNot.f(true));
        assertFalse(logicalNot.f(false));
        assertTrue(logicalNot.f(true));
    }

    /**
     * Test the valid {@link Not} logic.
     */
    @Test
    public void testNotValidNoDelay() {
        when(logicalNot.getDelay()).thenReturn(0);
        logicalNot.f(false);

        assertFalse(logicalNot.f(true));
        assertTrue(logicalNot.f(false));
        assertFalse(logicalNot.f(true));
    }

    /**
     * Tests a {@link Not} with 3 parameters.
     */
    @Test
    public void test3NotValidWithDelay() {
        when(logicalNot.getDelay()).thenReturn(1);
        logicalNot.f(true, true, false);
        assertFalse(logicalNot.f(false, true, false));
        assertTrue(logicalNot.f(true, true, false));
        assertFalse(logicalNot.f(false, true, false));

    }

    /**
     * Tests the correct implementation of toString.
     */
    @Test
    public void testToString() {
    	GateFactory gateFactory = new GateFactory();
    	IGate not = gateFactory.getNot();
        assertEquals("Not(x1, x2, x3)", not.toString("x1", "x2", "x3"));
        assertEquals("Not(x1, x2)", not.toString("x1", "x2"));
        assertEquals("Not(x1)", not.toString("x1"));
    }
}

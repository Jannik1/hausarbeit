package gate_test.implementation_test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import simulation.gate.exception.InvalidInputException;
import simulation.gate.implementation.NOT;

public class NOTTest {
	

	@Test(expected = InvalidInputException.class)
	public void exceptionWithoutParamNOTTest() {
		new NOT();
	}

	@Test
	public void validNOTTest() {
		NOT logicNot = new NOT(false);
		
		assertEquals(true, logicNot.f(true));
		assertEquals(false, logicNot.f(false));
		assertEquals(true, logicNot.f(true));
	}
	
	@Test(expected = InvalidInputException.class)
	public void exceptionWithThreeParamANDTest() {
		new NOT(true, false);
	}

}

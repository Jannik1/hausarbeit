package utils_test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

import simulation.utils.HazardDetector;

public class HazardDetectorTest {

	@Test
	public void isGlitchedTest(){
		ArrayList<Boolean> resultStack = new ArrayList<>(Arrays.asList(false, true, true));
		assertEquals(false, HazardDetector.isGlitched(resultStack));
		resultStack.add(false);
		assertEquals(true, HazardDetector.isGlitched(resultStack));
	}
	
	@Test
	public void emptyResultStack() {
		ArrayList<Boolean> resultStack = new ArrayList<>();
		assertEquals(false, HazardDetector.isGlitched(resultStack));
	}
}

package simulation.gate;

import java.util.ArrayList;

import simulation.utils.HazardDetector;
import simulation.utils.ParamsUtils;

public class CircuitExecuter {
	
	/**
	 * Executes all possible input (from -> to) combinations of a circuit and check the results for hazard.
	 * If it finds a hazard it reports it to the console.
	 */
	public ArrayList<String> execute() {
		ArrayList<String> glitches = new ArrayList<>();
		
		for(ArrayList<Boolean> input : ParamsUtils.permutate(Circuit.getInputCount())) {
			Circuit circut = new Circuit(input);
			for (ArrayList<Boolean> to : ParamsUtils.getOneChangeVariations(input)) {
				if (HazardDetector.isGlitched(circut.execute(to))) {
					String str = "Glitch for: " + circut.toString() + " from: " + input + " to: " + to + " found!";
					glitches.add(str);
					System.err.println(str);
				}
			}
		}
		return glitches;
	}
}

package de.nordakademie.simulation.circuit.hazard;

import java.util.List;
import java.util.Optional;

/**
 * A glitch is an inconstancy of a circuitName.
 * {@link Glitch} represents a glitch/hazard inside a
 * {@link de.nordakademie.simulation.circuit.Circuit}
 */
public final class Glitch {

    /**
     * The starting configuration of the
     * {@link de.nordakademie.simulation.circuit.Circuit}.
     */
    private List<Boolean> fromInput;

    /**
     * The input for {@link de.nordakademie.simulation.circuit.Circuit}
     * that caused the glitch to happen.
     */
    private List<Boolean> toInput;

    /**
     * private constructor as this class has a builder class.
     *
     * @param inputConfig starting configuration of the
     *                    {@link de.nordakademie.simulation.circuit.Circuit}
     * @param glitchInput input of the
     *                    {@link de.nordakademie.simulation.circuit.Circuit}
     *                    that caused the glitch.
     */
    private Glitch(final List<Boolean> inputConfig,
                   final List<Boolean> glitchInput) {
        this.fromInput = inputConfig;
        this.toInput = glitchInput;
    }

    @Override
    public String toString() {
        return "Glitch found from config: "
                + fromInput.toString()
                + " -> "
                + toInput.toString();
    }

    /**
     * This the builder class for {@link Glitch}.
     */
    public static final class Builder {

        /**
         * The starting configuration of the
         * {@link de.nordakademie.simulation.circuit.Circuit}.
         */
        private List<Boolean> fromInput;

        /**
         * The input for {@link de.nordakademie.simulation.circuit.Circuit}
         * that causes the glitch to happen.
         */
        private List<Boolean> toInput;

        /**
         * stores if result of circuit is glitched.
         */
        private boolean isGlitch = false;

        /**
         * Builder Method to add a starting configuration.
         *
         * @param inputConfig configuration with which the glitch accrued.
         * @return the current instance of the builder.
         */
        public Builder withFromInput(final List<Boolean> inputConfig) {
            this.fromInput = inputConfig;
            return this;
        }

        /**
         * Builder Method to add the input with which the glitch accrued.
         *
         * @param glitchInput input with which the glitch accrued.
         * @return the current instance of the builder.
         */
        public Builder withToInput(final List<Boolean> glitchInput) {
            this.toInput = glitchInput;
            return this;
        }

        /**
         * Builder Method to determine if result is a glitch.
         *
         * @param result the result of the executed cirucit.
         * @return the current instance of the builder.
         */
        public Builder forResult(final List<Boolean> result) {
            isGlitch = isGlitched(result);
            return this;
        }

        /**
         * This method detects glitches for a list of states.
         *
         * @param states a list of all states a circuit created until
         *               it reached its final state.
         * @return if there is a glitch (more than one change) in the states.
         */
        private boolean isGlitched(final List<Boolean> states) {
            int stateChanges = 0;
            if (!states.isEmpty()) {
                boolean previousState = states.get(0);
                for (boolean state : states) {
                    if (previousState != state) {
                        stateChanges++;
                        previousState = state;
                    }
                }
            }
            return stateChanges > 1;
        }

        /**
         * Builds the {@link Glitch}.
         *
         * @return the build {@link Glitch}.
         */
        public Optional<Glitch> build() {
            if (isGlitch) {
                return Optional.of(new Glitch(fromInput, toInput));
            } else {
                return Optional.empty();
            }
        }
    }
}

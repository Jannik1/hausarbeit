package de.nordakademie.simulation.circuit.gate.impl;

import de.nordakademie.simulation.circuit.gate.Gate;

import java.util.stream.IntStream;

/**
 * An implementation of {@link Gate} for a logical AND.
 */
public class And extends Gate {

    @Override
    protected boolean out(final boolean... in) {
        return IntStream
                .range(0, in.length)
                .mapToObj(i -> in[i])
                .reduce(Boolean::logicalAnd)
                .orElse(false);
    }

    @Override
    public int getDelay() {
        return 2;
    }
}

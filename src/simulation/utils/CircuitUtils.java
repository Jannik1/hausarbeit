package simulation.utils;

import simulation.gate.IGate;

public class CircuitUtils {

	/**
	 * Calculates the maximum time a circuit might take to reach its final state.
	 * @param gates (the gates which given circuit contains)
	 * @return the maximum duration a circuit could need to reach its final state.
	 */
	public static int maxDuration(IGate... gates) {
		int result = 1;
		
		for(IGate gate : gates) {
			result = result + gate.getDelay();
		}
		
		return result;
	}
	
}

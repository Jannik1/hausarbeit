/**
 * Hosts all {@link de.nordakademie.simulation.circuit.gate.IGate}
 * implementations.
 */
package de.nordakademie.simulation.circuit.gate.impl;

package simulation.utils;

import java.util.ArrayList;

public class ParamsUtils {

	/**
	 * This Method creates all boolean variations of a
	 * 
	 * @param length
	 * @return s the result inside a list of lists (with the given length)
	 */
	public static ArrayList<ArrayList<Boolean>> permutate(int length) {
		return permutate(new ArrayList<>(), new ArrayList<>(), 1, length);
	}

	private static ArrayList<ArrayList<Boolean>> permutate(ArrayList<ArrayList<Boolean>> results,
			ArrayList<Boolean> temp, int index, int length) {
		if (index > length) {
			results.add(temp);
		} else {
			ArrayList<Boolean> tempTrue = new ArrayList<>(temp);
			tempTrue.add(true);
			permutate(results, tempTrue, index + 1, length);

			ArrayList<Boolean> tempFalse = new ArrayList<>(temp);
			tempFalse.add(false);
			permutate(results, tempFalse, index + 1, length);
		}
		return results;
	}

	/**
	 * This Method return a list of boolean combinations that differ by one
	 * change to the
	 * 
	 * @param input
	 *            (list of booleans)
	 * @return a list of booleans that differ by one against the input.
	 */
	public static ArrayList<ArrayList<Boolean>> getOneChangeVariations(ArrayList<Boolean> input) {
		ArrayList<ArrayList<Boolean>> oneChangeVariations = new ArrayList<>();

		for (int index = 0; index < input.size(); index++) {
			ArrayList<Boolean> copiedInput = new ArrayList<>(input);
			copiedInput.set(index, !copiedInput.get(index));
			oneChangeVariations.add(copiedInput);
		}

		return oneChangeVariations;
	}
}

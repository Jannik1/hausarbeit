package de.nordakademie.simulation.circuit;

import java.util.List;

/**
 * Circuit Interface.
 */
public interface ICircuit {

    /**
     * @return s the number of inputs the circuit has to receive.
     */
    int getInputCount();

    /**
     * Executes the circuit with a given input array.
     *
     * @param input the input variables with which the circuit will be executed.
     * @return all states in with the circuit was unit it reached a final state
     */
    List<Boolean> execute(List<Boolean> input);
}

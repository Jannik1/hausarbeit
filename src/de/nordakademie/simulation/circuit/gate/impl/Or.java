package de.nordakademie.simulation.circuit.gate.impl;

import java.util.stream.IntStream;

import de.nordakademie.simulation.circuit.gate.Gate;

/**
 * An implementation of {@link Gate} for a logical OR.
 */
public class Or extends Gate {

    @Override
    protected boolean out(final boolean... in) {
        return IntStream
                .range(0, in.length)
                .mapToObj(i -> in[i])
                .reduce(Boolean::logicalOr)
                .orElse(false);
    }

    @Override
    public int getDelay() {
        return 2;
    }
}
